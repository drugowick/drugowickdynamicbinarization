/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drugodynbin.processing;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 *
 * @author bruno
 */
public class ProcessingAlgorithms {
    
    private BufferedImage image;

    public ProcessingAlgorithms(BufferedImage srcImage) {        
        this.image = srcImage;
    }

    public BufferedImage grayscale() {
        for (int i = 0; i < this.image.getWidth(); i++) { // X axis
            for (int j = 0; j < this.image.getHeight(); j++) { // Y axis
                Color color = new Color(this.image.getRGB(i, j));

                int r = color.getRed();
                int g = color.getGreen();
                int b = color.getBlue();
                int grayLayer = Math.round(r*0.3f + g*0.59f + b*0.11f); //luminance algorithm
                //int grayLayer = (r + g + b)/3; //default
                Color gray = new Color(grayLayer, grayLayer, grayLayer);
                this.image.setRGB(i, j, gray.getRGB());
            }
        }
        
        return this.image;
    }
    
    public BufferedImage binarize(Color lightColor, Color darkColor,
            double limiarModifier) {
        
        int countPixelsAnalyzed=0;
        // Controls the quantity of neighbors analyzed.
        int countNeighbors = 0;
        // Stores the RGB sum of the neighbors.
        int neighborsRGB = 0;
        // Defines the limiar. Initially it's the highest possible value.
        int limiar = 255;
        // Stores the RGB sum of the whole image (considering neighborhood).
        long totalRGB = 0;
        
        // ====== GET IMAGE LOWPOINT (DEEPEST BLACK) ====== //
        // X axis
        for (int i = 2; i < this.image.getWidth() - 3; i++) { 
            // Y axis
            for (int j = 2; j < this.image.getHeight() - 3; j++) { 
                //neighborhood X axis (consider 25 neighbors to avoid black pixel)
                for (int k = i - 2; k < i + 3; k++) { 
                    //neighborhood Y axis (consider 25 neighbors to avoid black pixel)
                    for (int l = j - 2; l < j + 3; l++) {
                        // Get color from neighbors and add up onto neighborsRGB
                        Color color = new Color(this.image.getRGB(i, j));
                        int rgb = color.getRed(); //Get any layer because they have the same value
                        neighborsRGB = neighborsRGB + rgb;
                        countNeighbors++; //
                    }
                }
                // Calculate neighborhood average pixel value
                neighborsRGB = Math.round(neighborsRGB / countNeighbors);
                
                // Check if lower value found. If yes, set as new limiar.
                // But remember that here the limiar variable is still just the
                // lowest rgb value of the image (deepest black).
                if (neighborsRGB < limiar){
                        limiar = neighborsRGB;
                }
                
                // Increment the total RGB, that will be used to define the 
                // whole image average RGB value later.
                totalRGB = totalRGB + neighborsRGB;
                // Increment number of analyzed pixels, that will be used to 
                // define the whole image average RGB value later.
                countPixelsAnalyzed++;
                // Initialize inner loop variables for the next iteration.
                countNeighbors = 0;
                neighborsRGB = 0;
            } // End of Y axis.
        } // End of X axis.
        
        // Calculate whole image average RGB value.
        long averageRGB = Math.round(totalRGB / countPixelsAnalyzed);
        int white = lightColor.getRGB();
        int black = darkColor.getRGB();
        double truelimiar = limiar + ((averageRGB - limiar) * limiarModifier);
        //Binarization
        int blacks = 0;
        int whites = 0;
        for (int i = 0; i < this.image.getWidth(); i++) {
            for (int j = 0; j < this.image.getHeight(); j++) {
                Color color = new Color(this.image.getRGB(i, j));
                int rgb = color.getRed(); //Get any layer because they have the same value
                if (rgb < limiar + ((averageRGB - limiar) * limiarModifier)){
                    this.image.setRGB(i, j, black);
                    blacks++;
                } else {
                    this.image.setRGB(i, j, white);
                    whites++;
                }
                countPixelsAnalyzed++;
            }			
        }
        
        return this.image;
    }

    public BufferedImage getImage() {
        return image;
    }
}
